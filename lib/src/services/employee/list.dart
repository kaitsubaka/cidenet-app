import 'dart:convert';

import 'package:cidenet/src/constants/endpoint.dart';
import 'package:cidenet/src/models/employee.dart';
import 'package:http/http.dart' as http;

/// A service that stores and retrieves user settings.
///
/// By default, this class does not persist user settings. If you'd like to
/// persist the user settings locally, use the shared_preferences package. If
/// you'd like to store settings on a web server, use the http package.
class EmployeeListService {
  Future<List<Employee>> getEmployees(Map<String, String> params) async {
    Uri uri =
        Uri.parse("$urlEndpoint:$portEndpoint/$apiPathEndpoint/employees");
    uri = uri.replace(queryParameters: params);
    //uri = uri.replace(queryParameters: <String, dynamic>{'page': page});
    http.Response res = await http.get(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (res.statusCode == 200) {
      List<dynamic> data = jsonDecode(res.body);
      List<Employee> employees = data
          .map(
            (dynamic item) => Employee.fromJson(item),
          )
          .toList();

      return employees;
    } else {
      throw "Unable to retrieve employees.";
    }
  }
}

import 'package:cidenet/src/constants/endpoint.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

/// A service that stores and retrieves user settings.
///
/// By default, this class does not persist user settings. If you'd like to
/// persist the user settings locally, use the shared_preferences package. If
/// you'd like to store settings on a web server, use the http package.
class EmployeeDeleteService {
  /// Persists the user's preferred ThemeMode to local or remote storage.
  Future<void> deleteEmployee(String id) async {
    final http.Response response = await http.delete(
      Uri.parse('$urlEndpoint:$portEndpoint/$apiPathEndpoint/employees/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode != 204) {
      throw "Unable to delte employee.";
    }
  }
}

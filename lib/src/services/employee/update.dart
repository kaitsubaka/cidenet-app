import 'dart:convert';

import 'package:cidenet/src/constants/endpoint.dart';
import 'package:cidenet/src/models/employee.dart';
import 'package:http/http.dart' as http;

/// A service that stores and retrieves user settings.
///
/// By default, this class does not persist user settings. If you'd like to
/// persist the user settings locally, use the shared_preferences package. If
/// you'd like to store settings on a web server, use the http package.
class EmployeeUpdateService {
  /// Loads the User's preferred ThemeMode from local or remote storage.

  /// Persists the user's preferred ThemeMode to local or remote storage.
  Future<Employee> updateEmployee(int id, Employee employee) async {
    // Use the shared_preferences package to persist settings locally or the
    // http package to persist settings over the network.

    final response = await http.put(
      Uri.parse(
          "$urlEndpoint:$portEndpoint/$apiPathEndpoint/employees/${id.toString()}"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(employee),
    );
    if (response.statusCode == 200) {
      // If the server did return a 200 updated response,
      // then parse the JSON.
      return Employee.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to update employee.');
    }
  }
}

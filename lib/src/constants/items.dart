// ignore_for_file: constant_identifier_names

import 'package:flutter/foundation.dart';

enum IdendityDocTypes { None, TI, CC, PA, CE }

extension IdendityDocTypesVal on IdendityDocTypes {
  String get value => describeEnum(this);
  String get name {
    switch (this) {
      case IdendityDocTypes.TI:
        return "Tarjeta identidad";
      case IdendityDocTypes.CC:
        return "Cedula ciudadania";
      case IdendityDocTypes.PA:
        return "Pasaporte";
      case IdendityDocTypes.CE:
        return "Cedula extranjeria";
      case IdendityDocTypes.None:
        return "None";
    }
  }
}

enum Countries { None, CO, US }

extension CountryVal on Countries {
  String get value => describeEnum(this);
  String get name {
    switch (this) {
      case Countries.CO:
        return "Colombia";
      case Countries.US:
        return "Estados Unidos";
      case Countries.None:
        return "None";
    }
  }
}

enum Areas {
  None,
  Administracion,
  Financiera,
  Compras,
  Infraestructura,
  Operacion,
  TalentoHumano,
  ServiciosVarios,
  Other,
}

extension AreasVal on Areas {
  String get value {
    return toString().split('.').last;
  }
}

enum States {
  None,
  Activo,
  Inactivo,
}

extension StatesVal on States {
  String get value {
    return toString().split('.').last;
  }
}

import 'package:cidenet/src/constants/items.dart';
import 'package:cidenet/src/models/employee.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

/// A class that many Widgets can interact with to read user settings, update
/// user settings, or listen to user settings changes.
///
/// Controllers glue Data Services to Flutter Widgets. The EmployeeFilterController
/// uses the EmployeeFilterService to store and retrieve user settings.
class EmployeeFilterController with ChangeNotifier {
  EmployeeFilterController();
  // Make EmployeeFilterService a private variable so it is not used directly.

  // Make ThemeMode a private variable so it is not updated directly without
  // also persisting the changes with the EmployeeFilterService.
  Employee _filters = Employee();
  // <<<<---- custom validators ---->>>>
  final _importantValidator = MultiValidator([
    MaxLengthValidator(20,
        errorText: 'maximun length allowed is 20 characters'),
    PatternValidator(r'^[A-Z\s]*$', errorText: 'just uppercase allowed')
  ]);
  final _idValidator = MultiValidator([
    MaxLengthValidator(20,
        errorText: 'maximun length allowed is 20 characters'),
    PatternValidator(r'^[0-9a-zA-Z\-]*$',
        errorText: 'only alphanumeric characters allowed')
  ]);

  // Allow Widgets to read the view state.
  Employee get filters => _filters;
  set filters(value) => _filters = value;
  MultiValidator get importantValidator => _importantValidator;
  MultiValidator get idValidator => _idValidator;
  // some widgets for option panels
  List<DropdownMenuItem<String>> get optionsCountry =>
      Countries.values.map((Countries country) {
        return DropdownMenuItem<String>(
            value: country.value, child: Text(country.name));
      }).toList();
  List<DropdownMenuItem<String>> get optionDocTypes =>
      IdendityDocTypes.values.map((IdendityDocTypes idendityDocType) {
        return DropdownMenuItem<String>(
            value: idendityDocType.value, child: Text(idendityDocType.name));
      }).toList();
  List<DropdownMenuItem<String>> get optionAreas =>
      Areas.values.map((Areas area) {
        return DropdownMenuItem<String>(
            value: area.value, child: Text(area.value));
      }).toList();
  List<DropdownMenuItem<String>> get optionStates =>
      States.values.map((States state) {
        return DropdownMenuItem<String>(
            value: state.value, child: Text(state.value));
      }).toList();
  // Set new values
  /// Update and persist the id document typebased on the user's selection.
  Future<void> setIdentityDocType(String? newIdDocType) async {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newIdDocType == _filters.identityDocType) return;
    if (newIdDocType == IdendityDocTypes.None.value) {
      return _filters.identityDocType = null;
    }
    ;
    // Otherwise, store the new theme mode in memory
    _filters.identityDocType = newIdDocType;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  /// Update and persist the id country on the user's selection.
  Future<void> setCountry(String? newCountry) async {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newCountry == _filters.countryOfEmployment) return;
    if (newCountry == Countries.None.value) {
      return _filters.countryOfEmployment = null;
    }
    // Otherwise, store the new theme mode in memory
    _filters.countryOfEmployment = newCountry;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  Future<void> setFiltersState(String? state) async {
    // Dot not perform any work if new and old ThemeMode are identical
    if (state == _filters.state) return;
    if (state == States.None.value) {
      return _filters.state = null;
    }
    // Otherwise, store the new theme mode in memory
    _filters.state = state;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  // << ----- Setters for employe form ----- >>
  setFiltersFirstSurname(String? newFirstSurname) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newFirstSurname == _filters.firstSurname) return;

    // Otherwise, store the new theme mode in memory
    _filters.firstSurname = newFirstSurname;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setFiltersSencondSurname(String? newSecondSurname) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newSecondSurname == _filters.secondSurname) return;

    // Otherwise, store the new theme mode in memory
    _filters.secondSurname = newSecondSurname;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setFiltersEmail(String? email) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (email == _filters.email) return;

    // Otherwise, store the new theme mode in memory
    _filters.email = email;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setFiltersName(String? newName) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newName == _filters.name) return;

    // Otherwise, store the new theme mode in memory
    _filters.name = newName;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setFiltersOtherNames(String? newOtherNames) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newOtherNames == _filters.otherNames) return;

    // Otherwise, store the new theme mode in memory
    _filters.otherNames = newOtherNames;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setFiltersIdentityDocNum(String? newIdDocNum) {
    // Dot not perform any work if new and old ThemeMode are identical
    if (newIdDocNum == _filters.identityDocNumber) return;

    // Otherwise, store the new theme mode in memory
    _filters.identityDocNumber = newIdDocNum;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }
}

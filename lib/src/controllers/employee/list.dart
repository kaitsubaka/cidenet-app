import 'package:cidenet/src/controllers/employee/filter.dart';
import 'package:cidenet/src/models/employee.dart';
import 'package:cidenet/src/services/employee/list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

/// A class that many Widgets can interact with to read user settings, update
/// user settings, or listen to user settings changes.
///
/// Controllers glue Data Services to Flutter Widgets. The EmployeeListController
/// uses the SettingsService to store and retrieve user settings.
class EmployeeListController with ChangeNotifier {
  static const _pageSize = 10;
  EmployeeListController(this._employeeListService);
  final EmployeeListService _employeeListService;
  // Make ThemeMode a private variable so it is not updated directly without
  // also persisting the changes with the SettingsService.

  Future<void> getEmployees(int page, PagingController pagingController,
      EmployeeFilterController employeeFilterController) async {
    try {
      Employee filters = employeeFilterController.filters;
      final newItems = await _employeeListService.getEmployees(<String, String>{
        'page': page.toString(),
        'page_size': _pageSize.toString(),
        'name': filters.name ?? "",
        'other_names': filters.otherNames ?? "",
        'first_surname': filters.firstSurname ?? "",
        'second_surname': filters.secondSurname ?? "",
        'identity_doc_type': filters.identityDocType ?? "",
        'identity_doc_number': filters.identityDocNumber ?? "",
        'country_of_employment': filters.countryOfEmployment ?? "",
        'email': filters.email ?? "",
        'state': filters.state ?? "",
      });
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = page + 1;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }
}

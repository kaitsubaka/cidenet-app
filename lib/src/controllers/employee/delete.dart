import 'package:cidenet/src/services/employee/delete.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// A class that many Widgets can interact with to read user settings, update
/// user settings, or listen to user settings changes.
///
/// Controllers glue Data Services to Flutter Widgets. The EmployeeDeleteController
/// uses the SettingsService to store and retrieve user settings.
class EmployeeDeleteController with ChangeNotifier {
  EmployeeDeleteController(this._employeeDeleteService);
  final EmployeeDeleteService _employeeDeleteService;
  // Make ThemeMode a private variable so it is not updated directly without
  // also persisting the changes with the SettingsService.

  Future<void> deleteEmployee(int id) async {
    await _employeeDeleteService.deleteEmployee(id.toString());
    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }
}

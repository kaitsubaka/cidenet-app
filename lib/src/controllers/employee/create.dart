import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:cidenet/src/constants/items.dart';
import 'package:cidenet/src/models/employee.dart';
import 'package:cidenet/src/services/employee/create.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

/// A class that many Widgets can interact with to read user settings, update
/// user settings, or listen to user settings changes.
///
/// Controllers glue Data Services to Flutter Widgets. The EmployeeCreateController
/// uses the EmployeeCreateService to store and retrieve user settings.
class EmployeeCreateController with ChangeNotifier {
  EmployeeCreateController(this._employeeCreateService);
  // Make EmployeeCreateService a private variable so it is not used directly.
  final EmployeeCreateService _employeeCreateService;

  // Make ThemeMode a private variable so it is not updated directly without
  // also persisting the changes with the EmployeeCreateService.
  Employee _newEmployee = Employee();
  String _identityDocType = IdendityDocTypes.None.value;
  String _country = Countries.None.value;
  String _area = Areas.None.value;
  bool _buttonIsDisabled = false;
  // <<<<---- custom validators ---->>>>
  final _importantValidator = MultiValidator([
    RequiredValidator(errorText: 'input required'),
    MaxLengthValidator(20,
        errorText: 'maximun length allowed is 20 characters'),
    PatternValidator(r'^[A-Z\s]*$', errorText: 'just uppercase allowed')
  ]);
  final _idValidator = MultiValidator([
    RequiredValidator(errorText: 'input required'),
    MaxLengthValidator(20,
        errorText: 'maximun length allowed is 20 characters'),
    PatternValidator(r'^[0-9a-zA-Z\-]*$',
        errorText: 'only alphanumeric characters allowed')
  ]);

  // Allow Widgets to read the view state.
  Employee get newEmployee => _newEmployee;
  String get identityDocType => _identityDocType;
  String get country => _country;
  String get area => _area;
  MultiValidator get importantValidator => _importantValidator;
  MultiValidator get idValidator => _idValidator;
  bool get buttonIsDisabled => _buttonIsDisabled;
  // some widgets for option panels
  List<DropdownMenuItem<String>> get optionsCountry =>
      Countries.values.map((Countries country) {
        return DropdownMenuItem<String>(
            value: country.value, child: Text(country.name));
      }).toList();
  List<DropdownMenuItem<String>> get optionDocTypes =>
      IdendityDocTypes.values.map((IdendityDocTypes idendityDocType) {
        return DropdownMenuItem<String>(
            value: idendityDocType.value, child: Text(idendityDocType.name));
      }).toList();
  List<DropdownMenuItem<String>> get optionAreas =>
      Areas.values.map((Areas area) {
        return DropdownMenuItem<String>(
            value: area.value, child: Text(area.value));
      }).toList();

  // Set new values
  /// Update and persist the id document typebased on the user's selection.
  Future<void> setIdentityDocType(String? newIdDocType) async {
    if (newIdDocType == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newIdDocType == _identityDocType) return;
    // Otherwise, store the new theme mode in memory
    _identityDocType = newIdDocType;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  /// Update and persist the id country on the user's selection.
  Future<void> setCountry(String? newCountry) async {
    if (newCountry == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newCountry == _country) return;

    // Otherwise, store the new theme mode in memory
    _country = newCountry;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  Future<void> setArea(String? newArea) async {
    if (newArea == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newArea == _area) return;

    // Otherwise, store the new theme mode in memory
    _area = newArea;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  // << ----- Setters for employe form ----- >>
  setEmployeeFirstSurname(String? newFirstSurname) {
    if (newFirstSurname == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newFirstSurname == _newEmployee.firstSurname) return;

    // Otherwise, store the new theme mode in memory
    _newEmployee.firstSurname = newFirstSurname;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setEmployeeSencondSurname(String? newSecondSurname) {
    if (newSecondSurname == null) return;
    // Dot not perform any work if new and old ThemeMode are identical
    if (newSecondSurname == _newEmployee.secondSurname) return;

    // Otherwise, store the new theme mode in memory
    _newEmployee.secondSurname = newSecondSurname;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setEmployeeName(String? newName) {
    if (newName == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newName == _newEmployee.name) return;

    // Otherwise, store the new theme mode in memory
    _newEmployee.name = newName;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setEmployeeOtherNames(String? newOtherNames) {
    if (newOtherNames == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newOtherNames == _newEmployee.otherNames) return;

    // Otherwise, store the new theme mode in memory
    _newEmployee.otherNames = newOtherNames;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setEmployeeIdentityDocNum(String? newIdDocNum) {
    if (newIdDocNum == null) return;

    // Dot not perform any work if new and old ThemeMode are identical
    if (newIdDocNum == _newEmployee.identityDocNumber) return;

    // Otherwise, store the new theme mode in memory
    _newEmployee.identityDocNumber = newIdDocNum;

    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  setEmployeeAdmissionDate(DateTime? newDate) {
    if (newDate == null) return;
    // Dot not perform any work if new and old ThemeMode are identical
    if (newDate == _newEmployee.admissionDate) return;
    // Otherwise, store the new theme mode in memory
    _newEmployee.admissionDate = newDate;
    // Important! Inform listeners a change has occurred.
    notifyListeners();
  }

  //
  Future<DateTime> onShowDateDialog(context, currentValue) async {
    final date = await showDatePicker(
        context: context,
        firstDate: DateTime.now().subtract(const Duration(days: 30)),
        initialDate: currentValue ?? DateTime.now(),
        lastDate: DateTime.now());
    if (date != null) {
      final time = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
      );
      return DateTimeField.combine(date, time);
    } else {
      return currentValue;
    }
  }

  //
  Future<dynamic> createNewEmployee() async {
    _buttonIsDisabled = true;
    notifyListeners();
    // Otherwise, store the new theme mode in memory
    _newEmployee.countryOfEmployment = _country;
    _newEmployee.identityDocType = _identityDocType;
    _newEmployee.area = _area;
    _newEmployee = await _employeeCreateService.createEmployee(_newEmployee);
    _buttonIsDisabled = false;
    notifyListeners();
  }
}

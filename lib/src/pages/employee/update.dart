import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cidenet/src/constants/items.dart';
import 'package:cidenet/src/controllers/employee/update.dart';
import 'package:cidenet/src/services/employee/update.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';

/// Creates an employee in te system.
class EmployeeUpdateView extends StatefulWidget {
  EmployeeUpdateView({
    Key? key,
    required this.pagingController,
    required this.employeeId,
  }) : super(key: key);
  final PagingController pagingController;
  final EmployeeUpdateController employeeUpdateController =
      EmployeeUpdateController(EmployeeUpdateService());
  final int employeeId;
  @override
  _EmployeeUpdateState createState() => _EmployeeUpdateState();
}

/// Displays the various settings that can be customized by the user.
///
/// When a user changes a setting, the SettingsController is updated and
/// Widgets that listen to the SettingsController are rebuilt.
class _EmployeeUpdateState extends State<EmployeeUpdateView> {
  // final init
  final _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.onUserInteraction;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Update Employee ${widget.employeeId}'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          // Glue the SettingsController to the theme selection DropdownButton.
          //
          // When a user selects a theme from the dropdown list, the
          // SettingsController is updated, which rebuilds the MaterialApp.
          child: SingleChildScrollView(
            child: Stack(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'First Surname'),
                          onChanged: widget
                              .employeeUpdateController.setEmployeeFirstSurname,
                          validator: widget
                              .employeeUpdateController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration: const InputDecoration(
                              labelText: 'Second Surname'),
                          onChanged: widget.employeeUpdateController
                              .setEmployeeSencondSurname,
                          validator: widget
                              .employeeUpdateController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'First Name'),
                          onChanged:
                              widget.employeeUpdateController.setEmployeeName,
                          validator: widget
                              .employeeUpdateController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        /////////
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'Other names'),
                          onChanged: widget
                              .employeeUpdateController.setEmployeeOtherNames,
                          validator: MaxLengthValidator(50,
                              errorText:
                                  'maximun length allowed is 50 characters'),
                          autovalidateMode: _autoValidateMode,
                        ),
                        ////////
                        DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                              labelText: 'Country of employment'),
                          value: widget.employeeUpdateController.country,
                          onChanged: widget.employeeUpdateController.setCountry,
                          items: widget.employeeUpdateController.optionsCountry,
                        ),
                        /////////
                        DropdownButtonFormField<String>(
                          decoration:
                              const InputDecoration(labelText: 'Identity type'),
                          value:
                              widget.employeeUpdateController.identityDocType,
                          onChanged: widget
                              .employeeUpdateController.setIdentityDocType,
                          items: widget.employeeUpdateController.optionDocTypes,
                        ),
                        //////
                        TextFormField(
                          decoration: const InputDecoration(
                              labelText: 'Identity number'),
                          onChanged: widget.employeeUpdateController
                              .setEmployeeIdentityDocNum,
                          validator:
                              widget.employeeUpdateController.idValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        //////////////
                        DateTimeField(
                          format: DateFormat("yyyy/MM/dd HH:mm:ss"),
                          decoration: const InputDecoration(
                              helperText: "Admission date"),
                          autovalidateMode: _autoValidateMode,
                          onShowPicker:
                              widget.employeeUpdateController.onShowDateDialog,
                          onChanged: widget.employeeUpdateController
                              .setEmployeeAdmissionDate,
                          resetIcon: null,
                        ),
                        //////////////
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Current Date: ' +
                                  DateFormat("yyyy/MM/dd hh:mm:ss")
                                      .format(DateTime.now())),
                          enabled: false,
                        ),
                        //////////////
                        TextFormField(
                          decoration: InputDecoration(
                              labelText:
                                  'Status: ' + describeEnum(States.Activo)),
                          enabled: false,
                        ),
                        //////////////
                        DropdownButtonFormField<String>(
                          decoration: const InputDecoration(labelText: 'Area'),
                          value: widget.employeeUpdateController.area,
                          onChanged: widget.employeeUpdateController.setArea,
                          items: widget.employeeUpdateController.optionAreas,
                        ),
                        //////

                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: ElevatedButton(
                            onPressed: widget
                                    .employeeUpdateController.buttonIsDisabled
                                ? null
                                : () async {
                                    // Validate returns true if the form is valid, or false otherwise.
                                    if (_formKey.currentState!.validate()) {
                                      // If the form is valid, display a snackbar. In the real world,
                                      // you'd often call a server or save the information in a database.
                                      AwesomeDialog(
                                          context: context,
                                          dialogType: DialogType.WARNING,
                                          headerAnimationLoop: false,
                                          animType: AnimType.TOPSLIDE,
                                          showCloseIcon: true,
                                          closeIcon: const Icon(
                                              Icons.close_fullscreen_outlined),
                                          title: 'Warning',
                                          desc:
                                              'Are you sure you want to edit the Employee with id ${widget.employeeId} ?',
                                          btnCancelOnPress: () {},
                                          onDissmissCallback: (type) {
                                            debugPrint(
                                                'Dialog Dissmiss from callback $type');
                                          },
                                          btnOkOnPress: () async {
                                            await widget
                                                .employeeUpdateController
                                                .updateEmployee(
                                                    widget.employeeId);
                                            widget.pagingController.refresh();
                                            Navigator.pop(context);
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                  content: Text(
                                                      'Employee ${widget.employeeId} updated')),
                                            );
                                          }).show();
                                    }
                                  },
                            child: const Text('Update'),
                          ),
                        )
                      ],
                    )),
              ),
            ]),
          ),
        ));
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

import 'package:cidenet/src/constants/items.dart';
import 'package:cidenet/src/controllers/employee/create.dart';
import 'package:cidenet/src/services/employee/create.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

/// Creates an employee in te system.
class EmployeeCreateView extends StatefulWidget {
  EmployeeCreateView({
    Key? key,
  }) : super(key: key);
  final EmployeeCreateController employeeCreateController =
      EmployeeCreateController(EmployeeCreateService());
  @override
  _EmployeeCreateState createState() => _EmployeeCreateState();
}

// this will be horryble code but ill try to keep it legible
class _EmployeeCreateState extends State<EmployeeCreateView> {
  // final init
  final _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.onUserInteraction;
  // states
  // << --- build function --->>
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Stack(children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFormField(
                  inputFormatters: [
                    UpperCaseTextFormatter(),
                  ],
                  decoration: const InputDecoration(labelText: 'First Surname'),
                  onChanged:
                      widget.employeeCreateController.setEmployeeFirstSurname,
                  validator: widget.employeeCreateController.importantValidator,
                  autovalidateMode: _autoValidateMode,
                ),
                TextFormField(
                  inputFormatters: [
                    UpperCaseTextFormatter(),
                  ],
                  decoration:
                      const InputDecoration(labelText: 'Second Surname'),
                  onChanged:
                      widget.employeeCreateController.setEmployeeSencondSurname,
                  validator: widget.employeeCreateController.importantValidator,
                  autovalidateMode: _autoValidateMode,
                ),
                TextFormField(
                  inputFormatters: [
                    UpperCaseTextFormatter(),
                  ],
                  decoration: const InputDecoration(labelText: 'First Name'),
                  onChanged: widget.employeeCreateController.setEmployeeName,
                  validator: widget.employeeCreateController.importantValidator,
                  autovalidateMode: _autoValidateMode,
                ),
                /////////
                TextFormField(
                  inputFormatters: [
                    UpperCaseTextFormatter(),
                  ],
                  decoration: const InputDecoration(labelText: 'Other names'),
                  onChanged:
                      widget.employeeCreateController.setEmployeeOtherNames,
                  validator: MaxLengthValidator(50,
                      errorText: 'maximun length allowed is 50 characters'),
                  autovalidateMode: _autoValidateMode,
                ),
                ////////
                DropdownButtonFormField<String>(
                  decoration:
                      const InputDecoration(labelText: 'Country of employment'),
                  value: widget.employeeCreateController.country,
                  onChanged: widget.employeeCreateController.setCountry,
                  items: widget.employeeCreateController.optionsCountry,
                  validator: (value) => value == Countries.None.value
                      ? 'Country can not be None'
                      : null,
                ),
                /////////
                DropdownButtonFormField<String>(
                  decoration: const InputDecoration(labelText: 'Identity Type'),
                  value: widget.employeeCreateController.identityDocType,
                  onChanged: widget.employeeCreateController.setIdentityDocType,
                  items: widget.employeeCreateController.optionDocTypes,
                  validator: (value) => value == IdendityDocTypes.None.value
                      ? 'Identitity type can not be None'
                      : null,
                ),
                //////
                TextFormField(
                  decoration:
                      const InputDecoration(labelText: 'Identity number'),
                  onChanged:
                      widget.employeeCreateController.setEmployeeIdentityDocNum,
                  validator: widget.employeeCreateController.idValidator,
                  autovalidateMode: _autoValidateMode,
                ),
                //////////////
                DateTimeField(
                  format: DateFormat("yyyy/MM/dd HH:mm:ss"),
                  decoration:
                      const InputDecoration(helperText: "Admission date"),
                  autovalidateMode: _autoValidateMode,
                  initialValue:
                      widget.employeeCreateController.newEmployee.admissionDate,
                  onShowPicker:
                      widget.employeeCreateController.onShowDateDialog,
                  onChanged:
                      widget.employeeCreateController.setEmployeeAdmissionDate,
                  resetIcon: null,
                  validator: (currentDate) =>
                      currentDate == null ? "Date can not be empty" : null,
                ),
                //////////////
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Current Date: ' +
                          DateFormat("yyyy/MM/dd hh:mm:ss")
                              .format(DateTime.now())),
                  enabled: false,
                ),
                //////////////
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Status: ' + describeEnum(States.Activo)),
                  enabled: false,
                ),
                //////////////
                DropdownButtonFormField<String>(
                  decoration: const InputDecoration(labelText: 'Area'),
                  value: widget.employeeCreateController.area,
                  onChanged: widget.employeeCreateController.setArea,
                  items: widget.employeeCreateController.optionAreas,
                  validator: (value) =>
                      value == Areas.None.value ? 'Area can not be None' : null,
                ),
                //////

                Container(
                  margin: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
                    onPressed: widget.employeeCreateController.buttonIsDisabled
                        ? null
                        : () async {
                            // Validate returns true if the form is valid, or false otherwise.
                            if (_formKey.currentState!.validate()) {
                              // If the form is valid, display a snackbar. In the real world,
                              // you'd often call a server or save the information in a database.

                              await widget.employeeCreateController
                                  .createNewEmployee();
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                    content: Text(
                                        'Employee ${widget.employeeCreateController.newEmployee.id} created')),
                              );
                            }
                          },
                    child: const Text('Submit'),
                  ),
                )
              ],
            )),
      ),
    ]));
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

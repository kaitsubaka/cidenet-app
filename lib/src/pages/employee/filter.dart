import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cidenet/src/constants/items.dart';
import 'package:cidenet/src/controllers/employee/filter.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';

/// Creates an employee in te system.
class EmployeeFilterView extends StatefulWidget {
  const EmployeeFilterView(
      {Key? key,
      required this.pagingController,
      required this.employeeFilterController})
      : super(key: key);
  final PagingController pagingController;
  final EmployeeFilterController employeeFilterController;
  @override
  _EmployeeFilterState createState() => _EmployeeFilterState();
}

/// Displays the various settings that can be customized by the user.
///
/// When a user changes a setting, the SettingsController is updated and
/// Widgets that listen to the SettingsController are rebuilt.
class _EmployeeFilterState extends State<EmployeeFilterView> {
  // final init
  final _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.onUserInteraction;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Filters'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          // Glue the SettingsController to the theme selection DropdownButton.
          //
          // When a user selects a theme from the dropdown list, the
          // SettingsController is updated, which rebuilds the MaterialApp.
          child: SingleChildScrollView(
            child: Stack(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'First Surname'),
                          onChanged: widget
                              .employeeFilterController.setFiltersFirstSurname,
                          validator: widget
                              .employeeFilterController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration: const InputDecoration(
                              labelText: 'Second Surname'),
                          onChanged: widget.employeeFilterController
                              .setFiltersSencondSurname,
                          validator: widget
                              .employeeFilterController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'First Name'),
                          onChanged:
                              widget.employeeFilterController.setFiltersName,
                          validator: widget
                              .employeeFilterController.importantValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        /////////
                        TextFormField(
                          inputFormatters: [
                            UpperCaseTextFormatter(),
                          ],
                          decoration:
                              const InputDecoration(labelText: 'Other names'),
                          onChanged: widget
                              .employeeFilterController.setFiltersOtherNames,
                          validator: MaxLengthValidator(50,
                              errorText:
                                  'maximun length allowed is 50 characters'),
                          autovalidateMode: _autoValidateMode,
                        ),
                        ////////
                        DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                              labelText: 'Country of employment'),
                          value: widget.employeeFilterController.filters
                              .countryOfEmployment,
                          onChanged: widget.employeeFilterController.setCountry,
                          items: widget.employeeFilterController.optionsCountry,
                        ),
                        /////////
                        DropdownButtonFormField<String>(
                          decoration:
                              const InputDecoration(labelText: 'Identity type'),
                          value: widget
                              .employeeFilterController.filters.identityDocType,
                          onChanged: widget
                              .employeeFilterController.setIdentityDocType,
                          items: widget.employeeFilterController.optionDocTypes,
                        ),
                        //////
                        TextFormField(
                          decoration: const InputDecoration(
                              labelText: 'Identity number'),
                          onChanged: widget.employeeFilterController
                              .setFiltersIdentityDocNum,
                          validator:
                              widget.employeeFilterController.idValidator,
                          autovalidateMode: _autoValidateMode,
                        ),
                        //////
                        TextFormField(
                          decoration: const InputDecoration(labelText: 'Email'),
                          onChanged:
                              widget.employeeFilterController.setFiltersEmail,
                          validator:
                              widget.employeeFilterController.idValidator,
                          autovalidateMode: _autoValidateMode,
                        ),

                        //////////////
                        TextFormField(
                          decoration: InputDecoration(
                              labelText:
                                  'Status: ' + describeEnum(States.Activo)),
                          enabled: false,
                        ),

                        //////////////
                        DropdownButtonFormField<String>(
                          decoration:
                              const InputDecoration(labelText: 'Status'),
                          value: widget.employeeFilterController.filters.state,
                          onChanged:
                              widget.employeeFilterController.setFiltersState,
                          items: widget.employeeFilterController.optionStates,
                        ),
                        //////

                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: ElevatedButton(
                            onPressed: () async {
                              // Validate returns true if the form is valid, or false otherwise.
                              if (_formKey.currentState!.validate()) {
                                // If the form is valid, display a snackbar. In the real world,
                                // you'd often call a server or save the information in a database.
                                Navigator.pop(context);
                                widget.pagingController.refresh();
                              }
                            },
                            child: const Text('Filter'),
                          ),
                        )
                      ],
                    )),
              ),
            ]),
          ),
        ));
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

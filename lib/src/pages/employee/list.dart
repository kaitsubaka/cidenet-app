import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cidenet/src/controllers/employee/delete.dart';
import 'package:cidenet/src/controllers/employee/filter.dart';
import 'package:cidenet/src/controllers/employee/list.dart';
import 'package:cidenet/src/pages/employee/filter.dart';
import 'package:cidenet/src/pages/employee/update.dart';
import 'package:cidenet/src/services/employee/delete.dart';
import 'package:cidenet/src/services/employee/list.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../models/employee.dart';

// /// Displays a list of Employees.
class EmployeeListView extends StatefulWidget {
  const EmployeeListView({
    Key? key,
  }) : super(key: key);

  @override
  _EmployeeListViewState createState() => _EmployeeListViewState();
}

class _EmployeeListViewState extends State<EmployeeListView> {
  final EmployeeListController _employeeListController =
      EmployeeListController(EmployeeListService());
  final EmployeeDeleteController _employeeDeleteController =
      EmployeeDeleteController(EmployeeDeleteService());
  final EmployeeFilterController _employeeFilterController =
      EmployeeFilterController();
  final PagingController<int, Employee> _pagingController =
      PagingController(firstPageKey: 1);
  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) async {
      await _employeeListController.getEmployees(
          pageKey, _pagingController, _employeeFilterController);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      // Don't worry about displaying progress or error indicators on screen; the
      // package takes care of that. If you want to customize them, use the
      // [PagedChildBuilderDelegate] properties.

      RefreshIndicator(
        onRefresh: () => Future.sync(
            // 2
            () => _pagingController.value = const PagingState<int, Employee>(
                nextPageKey: 0, error: null, itemList: null)),
        // 3
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: ElevatedButton(
                onPressed: () async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EmployeeFilterView(
                        employeeFilterController: _employeeFilterController,
                        pagingController: _pagingController,
                      ),
                    ),
                  );
                },
                child: const Text('Filter'),
              ),
            ),
            PagedSliverList<int, Employee>(
              pagingController: _pagingController,
              builderDelegate: PagedChildBuilderDelegate<Employee>(
                animateTransitions: true,
                itemBuilder: (context, item, index) => Card(
                  child: ListTile(
                    isThreeLine: true,
                    subtitle: Text('Other names: ${item.otherNames} \n'
                        'Country: ${item.countryOfEmployment} \n'
                        'Doc Type: ${item.identityDocType} \n'
                        'Doc Num: ${item.identityDocNumber} \n'
                        'Mail: ${item.email} \n'
                        'Area: ${item.area} \n'
                        'State: ${item.state} \n'
                        'Admission Date: ${DateFormat("yyyy/MM/dd hh:mm:ss").format(item.admissionDate!)} \n'),
                    title: Text(
                        'Employee ${item.id} - ${item.name} ${item.firstSurname} ${item.secondSurname}'),
                    leading: const CircleAvatar(
                      // Display the Flutter Logo image asset.
                      foregroundImage: AssetImage('assets/images/employee.jpg'),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        IconButton(
                          icon: const Icon(Icons.edit),
                          tooltip: 'Edit',
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EmployeeUpdateView(
                                  employeeId: item.id!,
                                  pagingController: _pagingController,
                                ),
                              ),
                            );
                          },
                        ),
                        IconButton(
                          icon: const Icon(Icons.delete),
                          tooltip: 'Delete',
                          onPressed: () {
                            AwesomeDialog(
                                context: context,
                                dialogType: DialogType.WARNING,
                                headerAnimationLoop: false,
                                animType: AnimType.TOPSLIDE,
                                showCloseIcon: true,
                                closeIcon:
                                    const Icon(Icons.close_fullscreen_outlined),
                                title: 'Warning',
                                desc:
                                    'Are you sure you want to delete the Employee with id ${item.id}?',
                                btnCancelOnPress: () {},
                                onDissmissCallback: (type) {
                                  debugPrint(
                                      'Dialog Dissmiss from callback $type');
                                },
                                btnOkOnPress: () async {
                                  await _employeeDeleteController
                                      .deleteEmployee(item.id!);
                                  _pagingController.refresh();
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Employee deleted')),
                                  );
                                  _employeeFilterController.filters =
                                      Employee();
                                }).show();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

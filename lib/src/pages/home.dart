import 'package:cidenet/src/controllers/employee/create.dart';
import 'package:cidenet/src/controllers/employee/delete.dart';
import 'package:cidenet/src/controllers/employee/list.dart';

import 'package:cidenet/src/pages/employee/create.dart';
import 'package:cidenet/src/pages/employee/list.dart';
import 'package:cidenet/src/pages/settings.dart';
import 'package:cidenet/src/services/employee/create.dart';
import 'package:cidenet/src/services/employee/delete.dart';
import 'package:cidenet/src/services/employee/list.dart';
import 'package:flutter/material.dart';

/// Displays a the home entry of Employees.
class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);
  static const routeName = '/';
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeView> {
  final PageController _pageController = PageController();
  String _title = "List of employees";
  int _index = 0;
  List<Widget> tabPages = [
    EmployeeListView(),
    EmployeeCreateView(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              // Navigate to the settings page. If the user leaves and returns
              // to the app after it has been killed while running in the
              // background, the navigation stack is restored.
              Navigator.restorablePushNamed(context, SettingsView.routeName);
            },
          ),
        ],
      ),
      body: PageView(
        children: tabPages,
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        onTap: onTabTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.list), label: "List Employees"),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), label: "Add Employee"),
        ],
      ),
    );
  }

  void onPageChanged(int page) {
    setState(() {
      _index = page;
      switch (_index) {
        case 0:
          _title = "List of Employees";
          break;
        case 1:
          _title = "Create Employee";
          break;
        default:
          _title = "List of Employees";
          break;
      }
    });
  }

  void onTabTapped(int index) {
    _pageController.animateToPage(index,
        duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
  }
}

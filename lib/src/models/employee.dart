import 'package:intl/intl.dart';

/// A placeholder class that represents an entity or model.
class Employee {
  Employee({
    this.id,
    this.firstSurname,
    this.secondSurname,
    this.name,
    this.otherNames,
    this.countryOfEmployment,
    this.identityDocType,
    this.identityDocNumber,
    this.email,
    this.area,
    this.state,
    this.admissionDate,
    this.createdAt,
    this.updatedAt,
  });
  int? id;
  String? firstSurname;
  String? secondSurname;
  String? name;
  String? otherNames;
  String? countryOfEmployment;
  String? identityDocType;
  String? identityDocNumber;
  String? email;
  String? area;
  String? state;
  DateTime? admissionDate;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      id: json['ID'],
      firstSurname: json['firstSurname'],
      secondSurname: json['secondSurname'],
      name: json['name'],
      otherNames: json['otherNames'],
      countryOfEmployment: json['countryOfEmployment'],
      identityDocType: json['identityDocType'],
      identityDocNumber: json['identityDocNumber'],
      email: json['email'],
      area: json['area'],
      state: json['state'],
      admissionDate:
          DateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'").parse(json['admissionDate']),
      createdAt: DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSz").parse(
          json['CreatedAt']
              .toString()
              .replaceFirstMapped(RegExp(r"(\.\d{6})\d+"), (m) => m[1]!)),
      updatedAt: DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSz").parse(
          json['UpdatedAt']
              .toString()
              .replaceFirstMapped(RegExp(r"(\.\d{6})\d+"), (m) => m[1]!)),
    );
  }
  Map<String, dynamic> toJson() => {
        'id': id,
        'firstSurname': firstSurname,
        'secondSurname': secondSurname,
        'name': name,
        'otherNames': otherNames,
        'countryOfEmployment': countryOfEmployment,
        'identityDocType': identityDocType,
        'identityDocNumber': identityDocNumber,
        'email': email,
        'area': area,
        'state': state,
        'admissionDate': admissionDate != null
            ? DateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'").format(admissionDate!)
            : null,
      };
}
